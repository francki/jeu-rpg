#include "../include/my.h"
#include "../include/rpg.h"

void	ball_dragon(use_value *all)
{
	if (all->x_ball+24 > all->x_dragon &&
	all->y_ball+24 < all->y_dragon + 53 &&
	all->x_ball+24 < all->x_dragon + 47 &&
	all->y_ball+24 > all->y_dragon) {
		if (all->stroke == 0) {
			all->x_dragon = 100;
			all->y_dragon = 100;
			all->str_shield =
			inttochar(my_getnbr(all->str_shield)+10);
			sfText_setString(all->shield_nbr, all->str_shield);
			all->stroke = all->save_stroke;
		}
		all->x_ball = 100000;
		all->y_ball = 100000;
		all->stroke--;
	}
}

int	hitbox_dragon(use_value *all)
{
	ball_dragon(all);
	if (all->x_perso+20 > all->x_dragon &&
	all->y_perso+20 < all->y_dragon+53 &&
	all->x_perso+20 < all->x_dragon+47 &&
	all->y_perso+20 > all->y_dragon) {
		if (hitbox(all->x_perso+20, all->y_perso, all) == 0)
			all->x_dragon -= 20;
		all->str_life = inttochar(my_getnbr(all->str_life)-1);
		sfText_setString(all->life_nbr, all->str_life);
	}
}

void	move_dragon(use_value *all)
{
	if (all->x_dragon > all->x_perso &&
	hitbox(all->x_dragon-6,all->y_dragon,all) == 0)
		all->x_dragon -= 6, all->dragon_r.top = 54;
	if (all->x_dragon < all->x_perso &&
	hitbox(all->x_dragon+6,all->y_dragon,all) == 0)
		all->x_dragon += 6, all->dragon_r.top = 107;
	if (all->y_dragon > all->y_perso &&
	hitbox(all->x_dragon,all->y_dragon-6,all) == 0)
		all->y_dragon -= 6, all->dragon_r.top = 160;
	if (all->y_dragon < all->y_perso &&
	hitbox(all->x_dragon,all->y_dragon+6,all) == 0)
		all->y_dragon += 6, all->dragon_r.top = 0;
	if (all->dragon_r.left >= all->left_dragon + 94)
		all->dragon_r.left = all->left_dragon;
	else
		all->dragon_r.left += 47;
}

int	dragon(use_value *all)
{
	hitbox_dragon(all);
	object(all->dragon_s, (sfVector2f){all->x_dragon,all->y_dragon},
	all->dragon_r, all);
	all->dragon_t = sfClock_getElapsedTime(all->dragon_c);
	all->second_dragon = all->dragon_t.microseconds / 1000000.0;
	if (all->second_dragon > 0.1) {
		move_dragon(all);
		sfClock_restart(all->dragon_c);
	}
	return (0);
}
