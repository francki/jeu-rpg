#include "../include/my.h"
#include "../include/rpg.h"

void	set_sprite_three(use_value *all)
{
	all->panel_north = create_object("docs/panelnord.png",
	(sfVector2f){0,0}, all);
	all->level = create_object("docs/scene1-1.png", (sfVector2f){0,0}, all);
	all->level_relief = create_object("docs/vide.png",
	(sfVector2f){0,0}, all);
}

void	set_sprite_two(use_value *all)
{
	all->exit = create_object("docs/exit_button.png",
	(sfVector2f){0,0}, all);
	all->boss_s = create_object("docs/BOSSFINAL.png",
	(sfVector2f){0,0}, all);
	all->htp = create_object("docs/htp.png",
	(sfVector2f){0,0}, all);
	all->bubble_s = create_object("docs/bulle0.png",
	(sfVector2f){0,0}, all);
	all->pause = create_object("docs/pause.png",
	(sfVector2f){0,0}, all);
	all->keys_menu = create_object("docs/menu_setting.png",
	(sfVector2f){0,0}, all);
	all->sword = create_object("docs/epee.png", (sfVector2f){0,0}, all);
	all->axe = create_object("docs/hache.png", (sfVector2f){0,0}, all);
	all->perso_s = create_object("docs/perso1.png", (sfVector2f){0,0}, all);
	all->panel_south = create_object("docs/panelsud.png",
	(sfVector2f){0,0}, all);
	set_sprite_three(all);
}

void	set_sprite(use_value *all)
{
	all->pseudo_menu = create_object("docs/pseudo.png",
	(sfVector2f){0,0}, all);
	all->pnj = create_object("docs/pnj.png",
	(sfVector2f){0,0}, all);
	all->deser = create_object("docs/Desert.png",
	(sfVector2f){0,0}, all);
	all->menu = create_object("docs/menu.png", (sfVector2f){0,0}, all);
	all->dragon_s = create_object("docs/ennemis_dragon.png",
	(sfVector2f){0,0}, all);
	all->fire_ball = create_object("docs/bouledefeu.png",
	(sfVector2f){0,0}, all);
	all->inventory = create_object("docs/interface.png",
	(sfVector2f){0,0}, all);
	all->bagpack = create_object("docs/inventaire.png",
	(sfVector2f){0,0}, all);
	all->h = create_object("docs/h.png", (sfVector2f){0,0}, all);
	all->f = create_object("docs/f.png", (sfVector2f){0,0}, all);
	all->perso_menu = create_object("docs/perso_menu.png",
	(sfVector2f){0,0}, all);
	set_sprite_two(all);
}

void	set_text_two(use_value *all)
{
	all->shield_nbr = sfText_create();
	sfText_setFont(all->shield_nbr, all->font);
	sfText_setString(all->shield_nbr, all->str_shield);
	sfText_setCharacterSize(all->shield_nbr, 25);
	sfText_setColor(all->shield_nbr, sfWhite);
	sfText_setStyle(all->shield_nbr, sfTextRegular);
	all->life_nbr = sfText_create();
	sfText_setFont(all->life_nbr, all->font);
	sfText_setString(all->life_nbr, all->str_life);
	sfText_setCharacterSize(all->life_nbr, 25);
	sfText_setColor(all->life_nbr, sfWhite);
	sfText_setStyle(all->life_nbr, sfTextRegular);
}

void	set_text(use_value *all)
{
	all->font = sfFont_createFromFile("SONIC.TTF");
	all->text = sfText_create();
	sfText_setFont(all->text, all->font);
	sfText_setString(all->text, all->pseudo);
	sfText_setCharacterSize(all->text, 170);
	sfText_setColor(all->text, sfBlack);
	sfText_setStyle(all->text, sfTextRegular);
	all->cooldown_str = "3";
	all->cooldown = sfText_create();
	sfText_setFont(all->cooldown, all->font);
	sfText_setString(all->cooldown, all->cooldown_str);
	sfText_setCharacterSize(all->cooldown, 25);
	sfText_setColor(all->cooldown, sfWhite);
	sfText_setStyle(all->cooldown, sfTextRegular);
	set_text_two(all);
}
