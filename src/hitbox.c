#include "../include/my.h"
#include "../include/rpg.h"

int	set_level(use_value *all, int i)
{
	char *coord = my_str_to_wordtab(all->door[i], ':')[3];

	all->level = set_texture_switch(my_strcat("docs/scene",
	my_str_to_wordtab(all->door[i], ':')[2], ".png"),
	all->level, (sfVector2f){0,0});
	all->level_relief = set_texture_switch(my_str_to_wordtab
	(all->door[i], ':')
	[4], all->level_relief, (sfVector2f){0,0});
	all->x_perso = my_getnbr(my_str_to_wordtab(coord, ',')[0]);
	all->y_perso = my_getnbr(my_str_to_wordtab(coord, ',')[1]);
	all->scene = my_str_to_wordtab(all->door[i], ':')[2];
	all->x_map2 = my_getnbr(my_str_to_wordtab(my_str_to_wordtab
	(all->door[i], ':')[5], ',')[0]);
	all->y_map2 = my_getnbr(my_str_to_wordtab(my_str_to_wordtab
	(all->door[i], ':')[5], ',')[1]);
	insert_hitbox(all);
}

int	check_coord(use_value *all, int i, int x, int y)
{
	int a = 0;
	int z = my_getnbr(my_str_to_wordtab(my_str_to_wordtab
	(all->door[i], ':')[1], ',')[0]);
	int e = my_getnbr(my_str_to_wordtab(my_str_to_wordtab
	(all->door[i], ':')[1], ',')[1]);
	int r = my_getnbr(my_str_to_wordtab(my_str_to_wordtab
	(all->door[i], ':')[1], ',')[2]);
	int t = my_getnbr(my_str_to_wordtab(my_str_to_wordtab
	(all->door[i], ':')[1], ',')[3]);
	for (a = 0 ; all->coord_door[a] ; a += 4) {
		if (x+35 > z + all->x_map2 &&
		y+55 < e + all->y_map2 &&
		x+25 < r + all->x_map2 &&
		y+64 > t + all->y_map2) {
			set_level(all, i);
			return (1);
		}
	}
	return (0);
}

int	hitbox(int x, int y, use_value *all)
{
	int fd; char *buff; char **av; int i = 0;

	while (all->door[i]) {
		if (my_strcmp(all->scene,
			my_str_to_wordtab(all->door[i], ':')[0]) == 0)
			if (check_coord(all, i, all->x_perso, all->y_perso)== 1)
				return (1);
		i++;
	} i = 0; if (x+35 > all->x_pnj + all->x_map2 &&
			y+55 < all->y_pnj + 61 + all->y_map2 &&
			x+25 < all->x_pnj + 56 + all->x_map2 &&
			y+64 > all->y_pnj + all->y_map2) {
		return (1); }
	while (all->hitbox[i]) {
		if (x+35 > my_getnbr(all->hitbox[i]) + all->x_map2 &&
		y+55 < my_getnbr(all->hitbox[i+1]) + all->y_map2 &&
		x+25 < my_getnbr(all->hitbox[i+2]) + all->x_map2 &&
		y+64 > my_getnbr(all->hitbox[i+3]) + all->y_map2) return (1);
		i += 4;
	} return (0);
}
