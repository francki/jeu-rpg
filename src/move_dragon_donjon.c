#include "../include/my.h"
#include "../include/rpg.h"

void	key_dragon(use_value *all, int i)
{
	if (i == 1)
		if (hitbox(all->x_dragon-all->speed,
		all->y_dragon-all->speed, all) != 1)
			all->y_dragon -= all->speed;
	if (i == 2)
		if (hitbox(all->x_dragon+all->speed,
		all->y_dragon+all->speed, all) != 1)
			all->y_dragon += all->speed;
	if (i == 3)
		if (hitbox(all->x_dragon+all->speed,
		all->y_dragon+all->speed, all) != 1)
			all->x_dragon += all->speed;
	if (i == 4)
		if (hitbox(all->x_dragon-all->speed,
		all->y_dragon-all->speed, all) != 1)
			all->x_dragon -= all->speed;
}

void	key_boss(use_value *all, int i)
{
	if (i == 1)
		if (hitbox(all->x_boss-all->speed,
		all->y_boss-all->speed, all) != 1)
			all->y_boss -= all->speed;
	if (i == 2)
		if (hitbox(all->x_boss+all->speed,
		all->y_boss+all->speed, all) != 1)
			all->y_boss += all->speed;
	if (i == 3)
		if (hitbox(all->x_boss+all->speed,
		all->y_boss+all->speed, all) != 1)
			all->x_boss += all->speed;
	if (i == 4)
		if (hitbox(all->x_boss-all->speed,
		all->y_boss-all->speed, all) != 1)
			all->x_boss -= all->speed;
}
