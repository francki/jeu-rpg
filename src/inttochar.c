#include "../include/my.h"
#include "../include/rpg.h"

char	*inttochar(unsigned n)
{
	char *str = NULL;
	int s = 0;
	int i = n;
	char *zero = "0";
	if (n == 0)
		return (zero);
	while (i) {
		i /= 10;
		s++;
	}
	str = malloc((s+1)*sizeof(char));
	if (str == NULL)
		return NULL;
	for (i = s-1 ; i >= 0 ; i--, n /= 10)
		str[i] = n % 10 + '0';
	str[s] = '\0';
	return (str);
}
