#include "../include/my.h"
#include "../include/rpg.h"

void	draw_level(use_value *all)
{
	if (my_strcmp(all->scene, "1") == 0)
		level_one(all);
	if (my_strcmp(all->scene, "1-1") == 0)
		level_one_one(all);
	if (my_strcmp(all->scene, "1-2") == 0)
		level_one_two(all);
	if (my_strcmp(all->scene, "1-3") == 0)
		level_one_tree(all);
	if (my_strcmp(all->scene, "2") == 0)
		level_two(all);
	if (my_strcmp(all->scene, "2-1") == 0)
		level_two_one(all);
}
