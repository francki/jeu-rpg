#include "../include/my.h"
#include "../include/rpg.h"

int	event_keys(use_value *all)
{
	int x = 0; int y = 0;
	all->x_window = sfRenderWindow_getSize(all->window).x;
	all->y_window = sfRenderWindow_getSize(all->window).y;
	while (sfRenderWindow_pollEvent(all->window, &all->event)) {
		if (all->event.key.code == sfKeyEscape)
			sfRenderWindow_close(all->window);
		if (all->event.type == sfEvtMouseButtonPressed) {
			x = all->event.mouseButton.x;
			y = all->event.mouseButton.y;
		}
	}
	if (x > 109 && y < 455 && x < 329 && y > 391) all->volume--;
	if (x > 1300 && y < 460 && x < 1426 && y > 380) all->volume++;
	if (x > 1310 && y < 765 && x < 1436 && y > 680)
		if (all->x_window + 10 < 1920 || all->y_window + 10 < 1020) {
			all->x_window += 10; all->y_window += 10;
			sfRenderWindow_setSize(all->window,
			(sfVector2u){all->x_window, all->y_window});
		}
	if (x > 115 && y < 750 && x < 334 && y > 690)
		if (all->x_window - 10 > 800 || all->y_window - 10 > 840) {
			all->x_window -= 10; all->y_window -= 10;
			sfRenderWindow_setSize(all->window,
			(sfVector2u){all->x_window, all->y_window});
		} return (0);
}

int	keys_sub(use_value *all)
{
	if (event_keys(all) == 1)
		return (1);
	sfRenderWindow_clear(all->window, sfBlack);
	menu_clock(all);
	draw_sprite(all->deser, (sfVector2f){all->x_p, 0}, all);
	draw_sprite(all->deser, (sfVector2f){all->x_p+1600, 0}, all);
	draw_sprite(all->keys_menu, (sfVector2f){0,0}, all);
	sfRenderWindow_display(all->window);
	return (0);
}

int	keys_menu(use_value *all)
{
	while (sfRenderWindow_isOpen(all->window)) {
		if (keys_sub(all) == 1)
			return (0);
	}
}
