#include "../include/my.h"
#include "../include/rpg.h"

int	inventory(use_value *all)
{
	int x = 0;
	int y = 0;

	draw_sprite(all->inventory, (sfVector2f){x, y}, all);
	sfText_setPosition(all->life_nbr, (sfVector2f){130, 40});
	sfRenderWindow_drawText(all->window, all->life_nbr, NULL);
	sfText_setPosition(all->shield_nbr, (sfVector2f){130, 108});
	sfRenderWindow_drawText(all->window, all->shield_nbr, NULL);
	return (0);
}

int	bagpack(use_value *all)
{
	while (1) {
		while (sfRenderWindow_pollEvent(all->window, &all->event)) {
			if (all->event.key.code == sfKeyEscape ||
			all->event.type == sfEvtClosed) {
				sfRenderWindow_close(all->window);
				return (0);
			}
			if (all->event.key.code == sfKeyReturn)
				return (0);
		}
		draw_sprite(all->bagpack, (sfVector2f){500, 200}, all);
		draw_sprite(all->perso_s, (sfVector2f){730, 380}, all);
		sfRenderWindow_display(all->window);
	}
	return (0);
}
