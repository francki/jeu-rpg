#include "../include/my.h"
#include "../include/rpg.h"

int	main(void)
{
	use_value all;

	set_struct(&all);
	set_text(&all);
	set_sprite(&all);
	insert_hitbox(&all);
	return (game(&all));
}
