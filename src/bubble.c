#include "../include/my.h"
#include "../include/rpg.h"

int	bubble(use_value *all)
{
	int i = 0;
	if (my_strcmp(all->scene, "1-1") == 0) {
		if (all->bulle == 4)
			draw_sprite(all->bubble_s,
			(sfVector2f){all->x_perso-150, all->y_perso-100}, all);
		else
			draw_sprite(all->bubble_s, (sfVector2f){495, 257}, all);
		while (sfRenderWindow_pollEvent(all->window, &all->event)) {
			if (all->event.type == sfKeyE) {
				i = 1;
			}
		}
		if (i == 1 && all->bulle < 4) {
			all->bulle++;
			set_texture_switch(my_strcat("docs/bulle",
			inttochar(all->bulle), ".png"), all->bubble_s,
			(sfVector2f){0,0});
			i = 0;
		}
	}
}
