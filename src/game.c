#include "../include/my.h"
#include "../include/rpg.h"
int	htp(use_value *all)
{
	while (1) {
		draw_sprite(all->htp, (sfVector2f){0,0}, all);
		sfRenderWindow_display(all->window);
		while (sfRenderWindow_pollEvent(all->window, &all->event)) {
			if (all->event.key.code == sfKeyU)
				return (0);
		}
	}
}

int	p(use_value *all)
{
	int x = 0; int y = 0;
	while (1) {
		while (sfRenderWindow_pollEvent(all->window, &all->event))
			if (all->event.type == sfEvtMouseButtonPressed) {
				x = all->event.mouseButton.x;
				y = all->event.mouseButton.y;
			}
		if (x > 1175 && y < 730 && x < 1450 && y > 610) {
			sfRenderWindow_close(all->window); return (1); }
		if (x > 636 && y < 732 && x < 900 && y > 600) {
			sfRenderWindow_close(all->window);
			game(all); return (1);
		}
		if (x > 125 && y < 730 && x < 780 && y > 610) return (0);
		if (x > 50 && y < 110 && x < 600 && y > 35)
			htp(all);
		x = 0; y = 0;
		draw_sprite(all->pause, (sfVector2f){0,0}, all);
		sfRenderWindow_display(all->window);
	}
}

void	event(use_value *all)
{
	while (sfRenderWindow_pollEvent(all->window, &all->event)) {
		if (all->event.key.code == sfKeyEscape ||
			all->event.type == sfEvtClosed)
			sfRenderWindow_close(all->window);
		(all->event.key.code == sfKeyE) ?
			check_action(all), bubble(all) : 0;
		(all->event.key.code == sfKeyP) ? p(all) : 0;
		(all->event.key.code == sfKeyI) ? bagpack(all) : 0;
		(all->event.type == sfEvtMouseButtonPressed) ? attack(all) : 0;
		key_right(all); key_left(all); key_up(all); key_down(all);
		all->perso_t = sfClock_getElapsedTime(all->perso_c);
		all->second = all->perso_t.microseconds / 1000000.0;
		if (all->second > 0.1) {
			if (all->perso_r.left > 576-65)
				all->perso_r.left = 0;
			else
				all->perso_r.left += 64;
			sfClock_restart(all->perso_c);
		}
	}
}

int	game(use_value *all)
{
	set_struct(all);
	take_action(all);
	insert_door(all);
	menu_title(all);
	while (sfRenderWindow_isOpen(all->window)) {
		event(all);
		sfRenderWindow_clear(all->window, sfBlack);
		draw_level(all);
		xp(all);
		dead(all);
		pnj(all);
		perso(all);
		if (my_strcmp(all->scene, "2-1") == 0) {
			boss(all);
			dragon(all);
		}
		relief(all);
		pseudo(all, all->x_perso - 10, all->y_perso - 20);
		inventory(all);
		bubble(all);
		fire_ball(all);
		sfRenderWindow_display(all->window);
	}
}
