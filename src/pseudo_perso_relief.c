#include "../include/my.h"
#include "../include/rpg.h"

void	pseudo(use_value *all, int x, int y)
{
	sfText_setPosition(all->text, (sfVector2f){x, y});
	sfRenderWindow_drawText(all->window, all->text, NULL);
}

void	perso(use_value *all)
{
	object(all->perso_s, (sfVector2f){all->x_perso,all->y_perso},
	all->perso_r, all);
}

void	relief(use_value *all)
{
	draw_sprite(all->level_relief, (sfVector2f){0, 0}, all);
}
