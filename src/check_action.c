#include "../include/my.h"
#include "../include/rpg.h"

int	take_action(use_value *all)
{
	int fd;
	char *buff;
	char **tab;
	int e = 0; int i = 0; int a = 0;

	if ((fd = open("src/ressources/action", O_RDONLY)) == -1)
		return (84);
	while (buff = my_gnl(fd)) {
		if (buff[0] != '#' && buff[0] != '\0') {
			all->action_e[i] = buff;
			tab = my_str_to_wordtab(buff, ':');
			tab = my_str_to_wordtab(tab[1], ',');
			for (a = 0 ; tab[a] ; a++, e++)
				all->coord_action[e] = tab[a];
			i++;
		}
	}
	all->coord_action[e] = NULL;
	all->action_e[i] = NULL;
	return (0);
}

int	verif_action(use_value *all, int i)
{
	int x = my_getnbr(my_str_to_wordtab(my_str_to_wordtab
	(all->action_e[i], ':')[3], ',')[0]);
	int y = my_getnbr(my_str_to_wordtab(my_str_to_wordtab
	(all->action_e[i], ':')[3], ',')[1]);

	if (my_strcmp("draw", my_str_to_wordtab(all->action_e[i], ':')[4]) == 0)
		draw_action(all, (sfVector2f){x, y},
		my_str_to_wordtab(all->action_e[i], ':')[2]);
}

int	verif_level(use_value *all, int i)
{
	int a = 0;
	int z = my_getnbr(my_str_to_wordtab(my_str_to_wordtab
	(all->action_e[i], ':')[1], ',')[0]);
	int e = my_getnbr(my_str_to_wordtab(my_str_to_wordtab
	(all->action_e[i], ':')[1], ',')[1]);
	int r = my_getnbr(my_str_to_wordtab(my_str_to_wordtab
	(all->action_e[i], ':')[1], ',')[2]);
	int t = my_getnbr(my_str_to_wordtab(my_str_to_wordtab
	(all->action_e[i], ':')[1], ',')[3]);
	for (a = 0 ; all->coord_action[a] ; a += 4) {
		if (all->x_perso+35 > z + all->x_map2 &&
		all->y_perso+55 < e + all->y_map2 &&
		all->x_perso+25 < r + all->x_map2 &&
		all->y_perso+64 > t + all->y_map2) {
			verif_action(all, i);
			return (1);
		}
	}
	return (0);
}

int	check_action(use_value *all)
{
	int i = 0;

	while (all->action_e[i]) {
		if (my_strcmp(all->scene, my_str_to_wordtab
		(all->action_e[i], ':')[0]) == 0) {
			if (verif_level(all, i) == 1)
				return (1);
		}
		i++;
	}
	return (0);
}

int	draw_action(use_value *all, sfVector2f pos, char *path)
{
	sfTexture *texture;
	sfSprite *sprite;

	texture = sfTexture_createFromFile(path, NULL);
	sprite = sfSprite_create();
	sfSprite_setTexture(sprite, texture, sfTrue);
	all->action_t = sfClock_getElapsedTime(all->action_c);
	all->second_action = all->action_t.microseconds / 1000000.0;
	while (all->second_action < 3) {
		all->action_t = sfClock_getElapsedTime(all->action_c);
		all->second_action = all->action_t.microseconds / 1000000.0;
		sfSprite_setPosition(sprite, pos);
		sfRenderWindow_drawSprite(all->window, sprite, NULL);
		sfRenderWindow_display(all->window);
	}
	sfClock_restart(all->action_c);
	return (0);
}
