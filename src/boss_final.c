#include "../include/my.h"
#include "../include/rpg.h"

void	ball_boss(use_value *all)
{
	if (all->x_ball+24 > all->x_boss &&
	all->y_ball+24 < all->y_boss + 53 &&
	all->x_ball+24 < all->x_boss + 47 &&
	all->y_ball+24 > all->y_boss) {
		if (all->stroke_boss == 0) {
			sfRenderWindow_close(all->window);
		}
		all->x_ball = 100000;
		all->y_ball = 100000;
		all->stroke_boss--;
	}
}

void	hitbox_boss(use_value *all)
{
	ball_boss(all);
	if (all->x_perso+20 > all->x_boss &&
	all->y_perso+20 < all->y_boss+53 &&
	all->x_perso+20 < all->x_boss+47 &&
	all->y_perso+20 > all->y_boss) {
		if (hitbox(all->x_perso+20, all->y_perso, all) == 0)
			all->x_boss -= 20;
		all->str_life = inttochar(my_getnbr(all->str_life)-10);
		sfText_setString(all->life_nbr, all->str_life);
	}
}

void	move_boss(use_value *all)
{
	if (all->x_boss > all->x_perso &&
	hitbox(all->x_boss-6,all->y_boss,all) == 0)
		all->x_boss -= 6, all->boss_r.top = 48;
	if (all->x_boss < all->x_perso &&
	hitbox(all->x_boss+6,all->y_boss,all) == 0)
		all->x_boss += 6, all->boss_r.top = 48 * 2;
	if (all->y_boss > all->y_perso &&
	hitbox(all->x_boss,all->y_boss-6,all) == 0)
		all->y_boss -= 6, all->boss_r.top = 48 * 3;
	if (all->y_boss < all->y_perso &&
	hitbox(all->x_boss,all->y_boss+6,all) == 0)
		all->y_boss += 6, all->boss_r.top = 0;
	if (all->boss_r.left >= 97 * 2)
		all->boss_r.left = 0;
	else
		all->boss_r.left += 97;
}

int	boss(use_value *all)
{
	hitbox_boss(all);
	object(all->boss_s, (sfVector2f){all->x_boss,all->y_boss},
		all->boss_r, all);
	all->boss_t = sfClock_getElapsedTime(all->boss_c);
	all->second_boss = all->boss_t.microseconds / 1000000.0;
	if (all->second_boss > 0.1) {
		move_boss(all);
		sfClock_restart(all->boss_c);
	}
	return (0);
}
