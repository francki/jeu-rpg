#include "../include/my.h"
#include "../include/rpg.h"

int	select_box(use_value *all, int x_select, int y_select)
{
	if (x_select > 426 && y_select < 612 && x_select<588&&y_select > 337)
		draw_sprite(all->f, (sfVector2f){458,290}, all);
	else if (x_select > 908 && y_select < 616 &&
	x_select < 1100 && y_select > 330)
		draw_sprite(all->h, (sfVector2f){924,277}, all);
	else if (x_select > 1295 && y_select < 829 &&
	x_select < 1512 && y_select > 723)
		draw_sprite(all->exit, (sfVector2f){1288,714}, all);
	else {
		draw_sprite(all->f, (sfVector2f){10000,10000}, all);
		draw_sprite(all->h, (sfVector2f){10000,10000}, all);
	} if (all->x_click > 426 && all->y_click < 612 &&
	all->x_click < 588 && all->y_click > 337) {
		set_texture_switch("docs/perso2.png", all->perso_s,
		(sfVector2f){0,0}); return (1);
	} if (all->x_click > 908 && all->y_click < 616 &&
	all->x_click < 1100 && all->y_click > 330) {
		set_texture_switch("docs/perso1.png", all->perso_s,
		(sfVector2f){0,0}); return (1);
	} return (0);
}

int	event_perso(use_value *all)
{
	static int x_select = 0; static int y_select = 0;
	all->x_click = 0; all->y_click = 0;
	while (sfRenderWindow_pollEvent(all->window, &all->event)) {
		if (all->event.key.code == sfKeyEscape)
			sfRenderWindow_close(all->window);
		if (all->event.type == sfEvtMouseButtonPressed) {
			all->x_click = all->event.mouseButton.x;
			all->y_click = all->event.mouseButton.y;
		}
		x_select = sfMouse_getPositionRenderWindow(all->window).x;
		y_select = sfMouse_getPositionRenderWindow(all->window).y;
	}
	if (select_box(all, x_select, y_select) == 1) {
		menu(all);
		return (1);
	}
	if (all->x_click > 1295 && all->y_click < 832 &&
	all->x_click < 1509 && all->y_click > 740)
		return (2);
	return (0);
}

int	menu_perso(use_value *all)
{
	int a;
	while (sfRenderWindow_isOpen(all->window)) {
		sfRenderWindow_clear(all->window, sfBlack);
		menu_clock(all);
		draw_sprite(all->deser, (sfVector2f){all->x_p, 0}, all);
		draw_sprite(all->deser, (sfVector2f){all->x_p+1600, 0}, all);
		draw_sprite(all->perso_menu, (sfVector2f){0,0}, all);
		if ((a = event_perso(all)) != 0)
			return (a);
		sfRenderWindow_display(all->window);
	}
	return (0);
}
