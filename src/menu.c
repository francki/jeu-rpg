#include "../include/my.h"
#include "../include/rpg.h"

int	add_char(use_value *all, char c)
{
	char *str;

	if (( str = malloc(sizeof(char) * 2)) == NULL)
		return (84);
	if (c > 'A' && c < 'z' && my_strlen(all->pseudo) < 7) {
		str[0] = c;
		str[1] = '\0';
		all->pseudo = my_strcat(all->pseudo, str, "\0");
	}
	return (0);
}

int	remove_char(use_value *all)
{
	int i = 0;

	if (my_strlen(all->pseudo) == 0)
		return (0);
	for (i = 0 ; all->pseudo[i] ; i++);
	all->pseudo[i - 1] = '\0';
	return (0);
}

int	event_pseudo(use_value *all)
{
	while (sfRenderWindow_pollEvent(all->window, &all->event)) {
		if (all->event.key.code == sfKeyEscape)
			sfRenderWindow_close(all->window);
		if (all->event.key.code == sfKeyReturn) {
			sfText_setColor(all->text, sfWhite);
			sfText_setCharacterSize(all->text, 16);
			return (1);
		}
		if (all->event.type == sfEvtKeyPressed) {
			if (all->event.key.code == sfKeyBack)
				remove_char(all);
			add_char(all, all->event.key.code + 65);
			sfText_setString(all->text, all->pseudo);
		}
	}
	return (0);
}

int	menu(use_value *all)
{
	while (sfRenderWindow_isOpen(all->window)) {
		if (event_pseudo(all) == 1)
			return (0);
		menu_clock(all);
		draw_sprite(all->deser, (sfVector2f){all->x_p, 0}, all);
		draw_sprite(all->deser, (sfVector2f){all->x_p+1600, 0}, all);
		draw_sprite(all->pseudo_menu, (sfVector2f){0, 0}, all);
		pseudo(all, 393, 400);
		sfRenderWindow_display(all->window);
	}
}
