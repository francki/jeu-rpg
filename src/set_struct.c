#include "../include/my.h"
#include "../include/rpg.h"

int	insert_door(use_value *all)
{
	int fd;	char *buff; char **tab;
	int e = 0; int i = 0; int a = 0;

	all->door = malloc(sizeof(char *) * 100);
	all->coord_door = malloc(sizeof(char *) * 100);
	if ((fd = open("src/ressources/door", O_RDONLY)) == -1)
		return (84);
	while (buff = my_gnl(fd)) {
		if (buff[0] != '#' && buff[0] != '\0') {
			all->door[i] = buff;
			tab = my_str_to_wordtab(buff, ':');
			tab = my_str_to_wordtab(tab[1], ',');
			for (a = 0 ; tab[a] ; a++, e++)
				all->coord_door[e] = tab[a];
			i++;
		}
	}
	all->coord_door[e] = NULL; all->door[i] = NULL;
	return (0);
}

int	insert_hitbox(use_value *all)
{
	int i = 0; char *buff; int a = 0; char **tab; int fd;

	if ((fd = open(my_strcat("src/ressources/scene",
	all->scene, "\0"), O_RDONLY)) == -1)
		return (84);
	while (buff = my_gnl(fd)) {
		if (buff[0] != '#' && buff[0] != '\0') {
			tab = my_str_to_wordtab(buff, ',');
			while (tab[a]) {
				all->hitbox[i] = tab[a];
				a++;
				i++;
			}
			a = 0;
		}
	}
	all->hitbox[i] = NULL;
	return (0);
}
