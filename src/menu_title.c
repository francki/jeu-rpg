#include "../include/my.h"
#include "../include/rpg.h"

int	menu_event(use_value *all)
{
	int x = 0; int y = 0;
	while (sfRenderWindow_pollEvent(all->window, &all->event)) {
		if (all->event.type == sfEvtMouseButtonPressed) {
			x = all->event.mouseButton.x;
			y = all->event.mouseButton.y;
		}
		if (x >= 685 && x <= 934 && y >= 368 && y <= 482) {
			if (menu_perso(all) == 1)
				return (1);
		}
		if (x >= 1322 && x <= 1515 && y >= 757 && y <= 830) {
			sfRenderWindow_close(all->window);
			return (1);
		}
		if (x >= 687 && x <= 931 && y >= 516 && y <= 627) {
			keys_menu(all);
			return (1);
		}
		x = 0, y = 0;
	} return (0);
}

int	menu_clock(use_value *all)
{
	all->perso_t = sfClock_getElapsedTime(all->perso_c);
	all->second = all->perso_t.microseconds / 1000000.0;
	if (all->second > 0.05) {
		all->x_p -= 1;
		sfClock_restart(all->perso_c);
	}
	return (0);
}

int	menu_parallax(use_value *all)
{
	int x_d = 0;
	int y_d = 0;

	x_d = sfMouse_getPositionRenderWindow(all->window).x;
	y_d = sfMouse_getPositionRenderWindow(all->window).y;
	if (x_d >= 685 && x_d <= 934 && y_d >= 368 && y_d <= 482)
		all->menu = set_texture_switch("docs/menu_play.png",
		all->menu, (sfVector2f){0,0});
	else if (x_d >= 1322 && x_d <= 1515 && y_d >= 757 && y_d <= 830)
		all->menu = set_texture_switch("docs/menu_exit.png",
		all->menu, (sfVector2f){0,0});
	else if (x_d >= 687 && x_d <= 931 && y_d >= 516 && y_d <= 627)
		all->menu = set_texture_switch("docs/menu_menu.png",
		all->menu, (sfVector2f){0,0});
	else
		all->menu = set_texture_switch("docs/menu.png",
		all->menu, (sfVector2f){0,0});
	return (0);
}

int	menu_sub(use_value *all)
{
	if (menu_event(all) == 1)
		return (1);
	menu_parallax(all);
	menu_clock(all);
	draw_sprite(all->deser, (sfVector2f){all->x_p, 0}, all);
	draw_sprite(all->deser, (sfVector2f){all->x_p+1600, 0}, all);
	draw_sprite(all->menu, (sfVector2f){0, 0}, all);
	sfRenderWindow_display(all->window);
	return (0);
}

int	menu_title(use_value *all)
{
	while (sfRenderWindow_isOpen(all->window))
		if (menu_sub(all) == 1)
			return (0);
	return (0);
}
