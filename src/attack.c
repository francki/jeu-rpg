#include "../include/my.h"
#include "../include/rpg.h"

int	attack(use_value *all)
{
	if (my_getnbr(all->cooldown_str) == 3) {
		all->fire_switch = 1;
		all->x_ball = all->x_perso + 10;
		all->y_ball = all->y_perso + 20;
		if (all->perso_r.top == 128)
			all->ball_trag = 128;
		else if (all->perso_r.top == 0)
			all->ball_trag = 0;
		else if (all->perso_r.top == 64)
			all->ball_trag = 64;
		else if (all->perso_r.top == 192)
			all->ball_trag = 192;
	}
}

int	clock_ball(use_value *all)
{
	all->fire_ball_t = sfClock_getElapsedTime(all->fire_ball_c);
	all->second = all->fire_ball_t.microseconds / 1000000.0;
	if (all->second > 0.01) {
		if (hitbox(all->x_ball, all->y_ball+6, all) == 1) {
			all->x_ball = 100000;
			all->y_ball = 100000;
		}
		(all->ball_trag == 128) ? all->y_ball += 6 : 0;
		(all->ball_trag == 0) ? all->y_ball -= 6 : 0;
		(all->ball_trag == 64) ? all->x_ball -= 6 : 0;
		(all->ball_trag == 192) ? all->x_ball += 6 : 0;
		if (all->fire_ball_r.left > all->left_ball + 98)
			all->fire_ball_r.left = all->left_ball;
		else
			all->fire_ball_r.left += 49;
		sfClock_restart(all->fire_ball_c);
	}
}

void	reset_cooldown(use_value *all)
{
	if (my_getnbr(all->cooldown_str) == 0) {
		all->fire_switch = 0;
		all->cooldown_str = "3";
		sfText_setString(all->cooldown, all->cooldown_str);
	}
}

int	fire_ball(use_value *all)
{
	sfText_setPosition(all->cooldown, (sfVector2f){105, 178});
	sfRenderWindow_drawText(all->window, all->cooldown, NULL);
	if (all->fire_switch == 1) {
		object(all->fire_ball, (sfVector2f){all->x_ball,
		all->y_ball}, all->fire_ball_r, all);
		all->cooldown_t = sfClock_getElapsedTime(all->cooldown_c);
		all->second_action = all->cooldown_t.microseconds / 1000000.0;
		if (all->second_action > 1) {
			all->cooldown_str =
			inttochar(my_getnbr(all->cooldown_str)-1);
			sfText_setString(all->cooldown, all->cooldown_str);
			sfClock_restart(all->cooldown_c);
			reset_cooldown(all);
		}
		clock_ball(all);
	}
}
