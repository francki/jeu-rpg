#include "../include/my.h"
#include "../include/rpg.h"

sfSprite	*create_object(const char *path, sfVector2f pos, use_value *use)
{
	sfTexture *texture;
	sfSprite *sprite;

	texture = sfTexture_createFromFile(path, NULL);
	sprite = sfSprite_create();
	sfSprite_setTexture(sprite, texture, sfTrue);
	sfSprite_setPosition(sprite, pos);
	return (sprite);
}

sfSprite	*set_texture_switch
(char *path, sfSprite *sprite, sfVector2f pos)
{
	sfTexture *texture;

	texture = sfTexture_createFromFile(path, NULL);
	sfSprite_setTexture(sprite, texture, sfTrue);
	sfSprite_setPosition(sprite, pos);
	return (sprite);
}

void	draw_sprite(sfSprite *sprite, sfVector2f pos, use_value *use)
{
	sfSprite_setPosition(sprite, pos);
	sfRenderWindow_drawSprite(use->window, sprite, NULL);
}

void	object
(sfSprite *sprite, sfVector2f pos, sfIntRect rect, use_value *use)
{
	sfSprite_setPosition(sprite, pos);
	sfSprite_setTextureRect(sprite, rect);
	sfRenderWindow_drawSprite(use->window, sprite, NULL);
}
