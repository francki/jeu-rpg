#include "../include/my.h"
#include <unistd.h>

char	**my_str_to_wordtab(char *str, char c)
{
	int	i;
	int	j;
	int	k;
	char	**tab;

	i = 0;
	k = 0;
	tab = malloc(my_strlen(str) * sizeof(*tab));
	while (str[k]) {
		j = 0;
		tab[i] = malloc(my_strlen(str) * sizeof(*tab[i]));
		while (str[k] != c && str[k] != 0)
			tab[i][j++] = str[k++];
		tab[i][j] = 0;
		if (str[k] != 0)
			k++;
		i++;
	}
	tab[i] = 0;
	return (tab);
}
