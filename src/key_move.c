#include "../include/my.h"
#include "../include/rpg.h"

void	key_down(use_value *all)
{
	if ((all->event.key.code == sfKeyDown ||
	all->event.key.code == sfKeyS) &&
	hitbox(all->x_perso, all->y_perso+all->speed, all) != 1) {
		if (my_strcmp(all->scene, "2-1") == 0) {
			all->y_map2 -= all->speed;
			key_dragon(all, 1);
			key_boss(all, 1);
		} else
			all->y_perso += all->speed;
		all->north_panel = 0;
		all->south_panel = 0;
		all->perso_r.top = 128;
	}

}

void	key_up(use_value *all)
{
	if ((all->event.key.code == sfKeyUp ||
	all->event.key.code == sfKeyZ) &&
	hitbox(all->x_perso, all->y_perso-all->speed, all) != 1) {
		if (my_strcmp(all->scene, "2-1") == 0) {
			all->y_map2 += all->speed;
			key_dragon(all, 2);
			key_boss(all, 2);
		} else
			all->y_perso -= all->speed;
		all->north_panel = 0;
		all->south_panel = 0;
		all->perso_r.top = 0;
	}
}

void	key_left(use_value *all)
{
	if ((all->event.key.code == sfKeyLeft ||
	all->event.key.code == sfKeyQ) &&
	hitbox(all->x_perso-all->speed, all->y_perso, all) != 1) {
		if (my_strcmp(all->scene, "2-1") == 0) {
			all->x_map2 += all->speed;
			key_dragon(all, 3);
			key_boss(all, 3);
		} else
			all->x_perso -= all->speed;
		all->north_panel = 0;
		all->south_panel = 0;
		all->perso_r.top = 64;
	}
}

void	key_right(use_value *all)
{
	if ((all->event.key.code == sfKeyRight ||
	all->event.key.code == sfKeyD) &&
	hitbox(all->x_perso+all->speed, all->y_perso, all) != 1) {
		if (my_strcmp(all->scene, "2-1") == 0) {
			all->x_map2 -= all->speed;
			key_dragon(all, 4);
			key_boss(all, 4);
		} else
			all->x_perso += all->speed;
		all->north_panel = 0;
		all->south_panel = 0;
		all->perso_r.top = 192;
	}
}
