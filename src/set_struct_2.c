#include "../include/my.h"
#include "../include/rpg.h"

void	set_struct_2(use_value *all)
{
	all->bulle = 0; all->volume = 10; all->x_p = 0;
	all->x_ball = 10; all->speed = 10; all->y_ball = 20;
	all->fire_switch = 0; all->str_shield = "0"; all->str_life = "100";
	all->perso_c = sfClock_create(); all->boss_c = sfClock_create();
	all->dragon_c = sfClock_create(); all->cooldown_c = sfClock_create();
	all->fire_ball_c = sfClock_create(); all->pnj_c = sfClock_create();
	all->action_c = sfClock_create(); all->x_pnj = 680;
	all->y_pnj = 350; all->x_map2 = 0;
	all->y_map2 = 0; all->spawn_x = 0;
	all->spawn_y = 0; all->x_perso = 784;
	all->y_perso = 629; all->scene = "1-1";
	all->north_panel = 0; all->south_panel = 0;
	all->perso_r.top = 0; all->perso_r.left = 0;
	all->perso_r.width = 64; all->perso_r.height = 64;
	all->boss_r.top = 0; all->boss_r.left = 0;
	all->boss_r.width = 97; all->boss_r.height = 48;
	all->fire_ball_r.top = 0; all->fire_ball_r.left = 0;
	all->fire_ball_r.width = 49; all->fire_ball_r.height = 49;
	all->pnj_r.top = 183; all->pnj_r.left = 0;
}

void	set_struct_3(use_value *all)
{
	all->pnj_r.width = 56;
	all->pnj_r.height = 61;
	all->dragon_r.top = 0;
	all->dragon_r.left = 0;
	all->left_dragon = 0;
	all->left_ball = 0;
	all->dragon_r.width = 47;
	all->dragon_r.height = 53;
	all->heart = 3;
	all->x_dragon = 300;
	all->y_dragon = 300;
	all->x_boss = 500;
	all->y_boss = 300;
}

int	set_struct(use_value *all)
{
	sfVideoMode	Mode = {1600, 900, 32};

	all->stroke_boss = 10;
	all->stroke = 3;
	all->save_stroke = all->stroke;
	all->pseudo = malloc(sizeof(char) * 100);
	all->hitbox = malloc(sizeof(char *) * 100);
	all->coord_action = malloc(sizeof(char *) * 100);
	all->action_e = malloc(sizeof(char *) * 100);
	all->window = sfRenderWindow_create
	(Mode, "RPG", sfResize | sfClose, NULL);
	all->pseudo = malloc(sizeof(char) * 7);
	memset(all->pseudo, '\0', 6);
	set_struct_2(all);
	set_struct_3(all);
	return (0);
}
