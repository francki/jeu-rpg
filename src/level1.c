#include "../include/my.h"
#include "../include/rpg.h"

void	level_one(use_value *all)
{
	draw_sprite(all->level, (sfVector2f){all->x_map2, all->y_map2}, all);
}

void	level_one_one(use_value *all)
{
	draw_sprite(all->level, (sfVector2f){all->x_map2, all->y_map2}, all);
}

void	level_one_two(use_value *all)
{
	draw_sprite(all->level, (sfVector2f){all->x_map2, all->y_map2}, all);
}

void	level_one_tree(use_value *all)
{
	draw_sprite(all->level, (sfVector2f){all->x_map2, all->y_map2}, all);
	draw_sprite(all->sword, (sfVector2f){186, 185}, all);
	draw_sprite(all->axe, (sfVector2f){1525, 597}, all);
}
