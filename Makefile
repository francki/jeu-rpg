SRC	=	src/*.c

NAME	=	my_rpg

all:	makelib $(NAME)

makelib :
		(cd ./lib && make re)

$(NAME) :
	gcc -g3 -o $(NAME) $(SRC) -lc_graph_prog -L lib -lmy

clean:
	rm -rf *~

fclean:	clean
	rm -rf $(NAME)
	(cd ./lib && make fclean)

re:	fclean all
