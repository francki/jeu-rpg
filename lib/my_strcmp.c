/*
** EPITECH PROJECT, 2017
** lib
** File description:
** my_strcmp
*/

#include "my.h"

int	my_strcmp(char *dest, char *src)
{
	int	a = 0;

	if (my_strlen(dest) != my_strlen(src))
		return (1);
	while (dest[a] && src[a]) {
		if (dest[a] > src[a])
			return (1);
		if (dest[a] < src[a])
			return (-1);
		a = a + 1;
	}
	return (0);
}
