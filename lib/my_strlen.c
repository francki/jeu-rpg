/*
** EPITECH PROJECT, 2017
** lib
** File description:
** my_strlen
*/

#include "my.h"

int	my_strlen(char *str)
{
	int	a = -1;

	if (!str)
		return (0);
	while (str[++a]);
	return (a);
}
