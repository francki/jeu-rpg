/*
** EPITECH PROJECT, 2017
** my_strcat.c
** File description:
** my_strcat.c
*/

#include "my.h"
#include <stdlib.h>

char	*my_strcat(char *strr, char *src, char *str)
{
	int	i = 0;
	int	j = 0;
	int	d = 0;
	char	*dest;

	if ((dest = malloc(sizeof(char) * 1000)) == NULL)
		return (NULL);
	while (strr[i] != '\0')
		dest[i] = strr[i], i = i + 1;
	while (src[j] != '\0') {
		dest[i + j] = src [j];
		j = j + 1;
	}
	while (str[d] != '\0') {
		dest[i+j+d] = str[d];
		d = d + 1;
	}
	dest[i + j + d] = '\0';
	return (dest);
}
