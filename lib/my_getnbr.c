/*
** EPITECH PROJECT, 2017
** lib
** File description:
** my_getnbr
*/

#include "my.h"

int	my_getnbr(char *str)
{
	int	a = 0;
	int	b = 0;

	if (str[0] == '-' && str[1])
		a = a + 1;
	while (str[a]) {
		if ((str[a] < '0' || str[a] > '9'))
			return (-1);
		b = b + str[a] - 48;
		b = b * 10;
		a = a + 1;
	}
	b = b / 10;
	if (str[0] == '-')
		return (-1 * b);
	else
		return (b);
}
