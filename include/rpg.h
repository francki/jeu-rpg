#include <string.h>

#ifndef RPG_H_
#define RPG_H_

typedef	struct	use_value
{
	int bulle;
	int volume;
	unsigned int x_window;
	unsigned int y_window;
	int x_p;
	int spawn_x;
	int spawn_y;
	int stroke;
	int stroke_boss;
	int save_stroke;
	int left_dragon;
	int left_ball;
	int ball_trag;
	int speed;
	sfText *text;
	sfText *cooldown;;
	sfText *life_nbr;
	sfText *shield_nbr;
	sfFont *font;
	char *cooldown_str;
	char *str_life;
	char *str_shield;
	char *pseudo;
	char **door;
	char **coord_door;
	char **action_e;
	char **coord_action;
	char **hitbox;
	sfSprite	*pnj;
	sfSprite	*htp;
	sfSprite	*dragon_s;
	sfIntRect	pnj_r;
	sfIntRect	dragon_r;
	sfIntRect	fire_ball_r;
	int x_pnj;
	int y_pnj;
	sfSprite	*pause;
	sfSprite	*bubble_s;
	sfSprite	*keys_menu;
	sfSprite	*menu;
	sfSprite	*exit;
	sfSprite	*inventory;
	sfSprite	*fire_ball;
	sfSprite	*h;
	sfSprite	*f;
	sfSprite	*bagpack;
	sfSprite	*perso_menu;
	sfSprite	*pseudo_menu;
	sfSprite	*sword;
	sfSprite	*axe;
	sfSprite	*perso_s;
	sfSprite	*chest_open;
	sfSprite	*level;
	sfSprite	*level_relief;
	sfSprite	*panel_north;
	sfSprite	*panel_south;
	sfSprite	*deser;
	sfSprite	*boss_s;
	int	south_panel;
	int	north_panel;
	sfRenderWindow	*window;
	sfIntRect	perso_r;
	sfIntRect	boss_r;
	sfEvent		event;
	int	x_boss;
	int	y_boss;
	int	x_perso;
	int	y_perso;
	int	x_ball;
	int	y_ball;
	sfClock	*perso_c;
	sfTime	perso_t;
	sfClock	*cooldown_c;
	sfTime	cooldown_t;
	sfClock	*dragon_c;
	sfTime	dragon_t;
	float second_dragon;
	sfClock	*boss_c;
	sfTime	boss_t;
	float second_boss;
	sfClock	*fire_ball_c;
	sfTime	fire_ball_t;
	float	second;
	sfClock	*pnj_c;
	sfTime	pnj_t;
	sfClock	*action_c;
	sfTime	action_t;
	float second_action;
	int	box;
	char	*scene;
	int x_dragon;
	int y_dragon;
	int x_map2;
	int y_map2;
	int x_click;
	int y_click;
	int heart;
	int fire_switch;
} use_value;

void	key_boss(use_value *all, int i);
int     insert_hitbox(use_value *all);
void    set_sprite(use_value *all);
void    set_text(use_value *all);
int	boss(use_value *all);

/*
** dragon
*/
int	dragon(use_value *all);
void	key_dragon(use_value *all, int i);

/*
** menu
*/
int	keys_menu(use_value *all);
int	menu_perso(use_value *all);
int	menu(use_value *all);
int	menu_clock(use_value *all);
int	menu_title(use_value *all);

/*
** perso
*/
int	bubble(use_value *all);
int	xp(use_value *all);
int	dead(use_value *all);
void	perso(use_value *all);
void	pseudo(use_value *all, int x, int y);
int	inventory(use_value *all);
int	pnj(use_value *all);
int	weapons(int x, int y, use_value *all);
void	key_right(use_value *all);
void	key_left(use_value *all);
void	key_down(use_value *all);
void	key_up(use_value *all);

/*
** outils
*/
char	*inttochar(unsigned n);
sfSprite        *create_object(const char *path,
sfVector2f pos, use_value *use);
sfSprite        *set_texture_switch(char *path,
sfSprite *sprite, sfVector2f pos);
void	object(sfSprite *sprite, sfVector2f pos,
sfIntRect rect, use_value *use);
char	**my_str_to_wordtab(char *str, char c);
char	**my_word_array(char *str, char sep, int i);
char	*my_gnl(const int fd);

/*
** attack
*/
int	fire_ball(use_value *all);
int	attack(use_value *all);
int	take_action(use_value *all);
int	draw_action(use_value *all, sfVector2f pos, char *path);

/*
** level
*/
void	level_one(use_value *all);
void	level_one_one(use_value *all);
void	level_one_two(use_value *all);
void	level_one_tree(use_value *all);
void	level_two(use_value *all);
void	level_two_one(use_value *all);
void	draw_level(use_value *all);
void	draw_sprite(sfSprite *sprite, sfVector2f pos, use_value *use);
int	draw_panel(use_value *all);
int	reset_chest(use_value *all);
int	draw_chest(use_value *all);
void	relief(use_value *all);
int	draw_panel(use_value *all);

/*
** init
*/
int	insert_door(use_value *all);
int	check_action(use_value *all);
int	chest(int x, int y, use_value *all);
int	insert_hitbox(use_value *all);
int	bagpack(use_value *all);
int	hitbox(int x, int y, use_value *all);
int	game(use_value *all);
int     set_struct(use_value *all);

#endif
